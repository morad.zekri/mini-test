<?php

namespace App\Tests;

use App\Entity\Facture;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;

class FactureTest extends TestCase
{
    public function testPrxTTCAutomatic(): void
    {
        $facture = new Facture();
        $facture->setPrixHT(100);

        $this->assertEquals(120, $facture->getPrixTTC());
    }

    public function testDesignationMore255(): void
    {
        $facture = new Facture();

        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();

        $facture->setDesignation($this->generateRandomString(256));
        $errors = (string) $validator->validate($facture);

        $this->assertStringContainsString(
            'Designation name cannot be longer than 255 characters',
            $errors
        );
    }

    public function testPriceMoreOneMilion()
    {
        $facture = new Facture();

        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();

        $facture->setPrixHT(100000);
        $errors = (string) $validator->validate($facture);

        $this->assertStringContainsString(
            'Price must be between 1 euro and 99999 euro',
            $errors
        );
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}
