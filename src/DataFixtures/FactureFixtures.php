<?php

namespace App\DataFixtures;

use App\Entity\Facture;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class FactureFixtures extends Fixture
{
    /**
     * @var UserPasswordHasherInterface
     */
    private $userPasswordHasher;

    public function __construct(UserPasswordHasherInterface $userPasswordHarsher)
    {
        $this->userPasswordHasher = $userPasswordHarsher;
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User();

        $user->setEmail('test@test.com');
        $user->setPassword($this->userPasswordHasher->hashPassword(
            $user,
           'test123'
       ));
        $manager->persist($user);

        $faker = Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 100; ++$i) {
            $facture = new Facture();
            $facture->setDesignation($faker->sentence($nbWords = 6, $variableNbWords = true));
            $facture->setDescription($faker->paragraph($nbSentences = 10));
            $facture->setPrixHT($faker->numberBetween($min = 15, $max = 600));

            $facture->setUser($user);

            $manager->persist($facture);
        }

        $manager->flush();
    }
}
