


## Install

to run the application please use this commands

    $ sudo docker-compose up -d
    $ sudo docker-compose exec php-fpm composer install
    $ sudo docker-compose exec php-fpm bin/console doctrine:database:create
    $ sudo docker-compose exec php-fpm bin/console doctrine:migrations:migrate
    $ sudo docker-compose exec php-fpm bin/console doctrine:fixtures:load

Add this ligne on your /etc/hosts :
##### `127.0.1.1       testrec01.local`

You can go to this link to visit the Apps :
http://testrec01.local:5458/

for the credientels : 

email : test@test.com
password: test123

## PHP Insights

to analyze the code you can run this command

    $ sudo docker-compose exec php-fpm ./vendor/bin/phpinsights -s

## CS FIXER

to run the cs clean fix please use this commands

    $ sudo docker-compose exec php-fpm mkdir --parents tools/php-cs-fixer
    $ sudo docker-compose exec php-fpm composer require --working-dir=tools/php-cs-fixer friendsofphp/php-cs-fixer
    $ sudo docker-compose exec php-fpm tools/php-cs-fixer/vendor/bin/php-cs-fixer fix src
    $ sudo docker-compose exec php-fpm tools/php-cs-fixer/vendor/bin/php-cs-fixer fix tests

## Testing

to run the testing please use this commands

    $ sudo docker-compose exec php-fpm bin/phpunit
